# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias cible='cd ~/Desktop/DOC_DEV_DISTRIBUTEUR/build/distrib-phyBOARD_Wega-Debug'
alias getqss='scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-Desktop_Qt_5_3_GCC_64bit-Debug/*.qss root@192.168.0.15:/home'


alias pvbox='sudo mount -t vboxsf vbox ~/Desktop/vbox'
alias distrib='cd ~/Desktop/DOC_DEV_DISTRIBUTEUR/distrib'
alias centrale='cd ~/Desktop/DOC_DEV_DISTRIBUTEUR/code_centrale/cmar76-optiweb-station-f4b983cb5fb8'
alias cls="echo -ne '\033c'"
alias brc='gedit ~/.bashrc'
alias rbrc='~/.bashrc'
alias pvbox='sudo mount -t vboxsf vbox ~/Desktop/vbox'
alias cmd='cat /home/phyvm/cmd_linux.txt'
alias hist='history'
alias goyoc='cd /opt/PHYTEC_BSPs/phyOPTIWEB'
alias env='source sources/poky/oe-init-build-env'
alias h='cd /home'
alias hb='cd ~/Desktop/HOME_BOULOT3'
alias cdb='mysql -h 192.168.0.11 -u distribHX -pmoussauto'
alias toPack='cd ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/toPack'

alias pqt='/opt/x64_Qt5.3.2/Tools/QtCreator/bin/qtcreator.sh'
alias lock='ssh root@192.168.0.15'
alias lockovh='ssh root@web-coonguu1.tst.fra.optiweb-access.com'
alias getimages='scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-Desktop_Qt_5_3_GCC_64bit-Debug/images/*.* root@192.168.0.15:/home/images'


# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


export PS1="\[\e[36;1m\]\u@\h:\w$ \[\e[0m\]"

#-------------------------------------------------------------------------------------------------------
lockto()
{
	ssh root@192.168.0.$1
}


getdistribto()
{
	scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-phyBOARD_Wega-Debug/DISTRIBUTEUR root@192.168.0.$1:/home
	scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-Desktop_Qt_5_3_GCC_64bit-Debug/*.qss root@192.168.0.$1:/home
}

sdcardto()
{
	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/firmware_micro_annexe/fuji_distri.0.2.17.mhx root@192.168.0.$1:/media/sdcard/updates/fuji_distri.0.2.17.mhx
}


#--------------------------------------------------------------------
senddistribovh()
{
	cp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-phyBOARD_Wega-Debug/DISTRIBUTEUR ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-phyBOARD_Wega-Debug/NEW_DISTRIBUTEUR
	scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-phyBOARD_Wega-Debug/NEW_DISTRIBUTEUR root@web-coonguu1.tst.fra.optiweb-access.com:/home
	scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-Desktop_Qt_5_3_GCC_64bit-Debug/*.qss root@web-coonguu1.tst.fra.optiweb-access.com:/home
	scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-Desktop_Qt_5_3_GCC_64bit-Debug/images/*.* root@web-coonguu1.tst.fra.optiweb-access.com:/home/images
}

#--------------------------------------------------------------------
getdistrib()
{
	scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-phyBOARD_Wega-Debug/DISTRIBUTEUR root@192.168.0.15:/home
	scp ~/Desktop/DOC_DEV_DISTRIBUTEUR/git/distributeur/distrib-Desktop_Qt_5_3_GCC_64bit-Debug/*.qss root@192.168.0.15:/home
}

getsystem()
{
	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/distrib_start.sh root@192.168.0.15:/home
	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/distrib_fct.sh root@192.168.0.15:/home
	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/config.txt root@192.168.0.15:/home
	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/new2dis.service root@192.168.0.15:/etc/systemd/system
	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/distrib_fct.service root@192.168.0.15:/etc/systemd/system
	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/libQt5SerialPort.so.5 root@192.168.0.15:/usr/lib

	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/eGTouchD root@192.168.0.15:/usr/bin
	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/eGTouchL.ini root@192.168.0.15:/etc

	scp ~/id_rsa_srv_ow root@192.168.0.15:/home
	scp ~/Desktop/HOME_BOULOT3/DISTRIBUTEUR_SYSTEME/config root@192.168.0.15:/home/.ssh
}

gogit()
{
	cd ~/Desktop/DOC_DEV_DISTRIBUTEUR/git
}


#cat ~/Desktop/HOME_BOULOT3/cmd_linux.txt

gethelp()
{
echo ""
echo "---- Commandes BASH pour environnement distributeur ---------------"
echo ""
echo "lock          : Verrouillage vers le distributeur DEV"
echo "getdistrib    : Transfert executable + qss vers le distributeur DEV"
echo "getsystem     : Transfert des fichiers systemes vers le distributeur DEV"
echo "getimages     : Transfert des images vers le distributeur DEV"
echo ""
echo "lockto        : Verrouillage vers l'I.P d'un autre distributeur (chiffre IP en parametre)"
echo "getdistribto  : Idem mais pour le transfert d'executable + qss"
echo "sdcardto      : Copie le fichier du micro annexe (IP en param, ne pas oublier de creer les repertoire a distance)"
echo ""
echo "lockovh       : ssh vers le serveur ovh coonguu"
echo "senddistribovh : Copie du dernir exe + qss vers le serveur ovh dans /home, avec DISTRIBUTEUR renommé en NEW_DISTR..."
echo ""
echo "gogit         : aller au repertoire git"
echo "-------------------------------------------------------------------"
echo ""
}

echo "Taper gethelp pour obtenir de l'aide sur les commandes disponibles"

getversion()
{
   grep --color=never g_version *paramLog.*
}

getname()
{
  echo ""
  echo "13 : MARINES"
  echo "22 : LES ECUELLES (VERNEUIL)"
  echo "24 : BERGON (BOURMES-LES-MIMOSAS"
  echo "27 : ROMOND"
  echo "28 : JEDAV (PACY SUR EURE)"
  echo ""
  echo " 6 : DEV"
  echo ""
}


alive()
{
  ls -al *paramLog.* | sed -e 's/13_paramLog.txt/MARINES/g' -e 's/22_paramLog.txt/LES ECUELLES/g' -e 's/24_paramLog.txt/BERGON/g' -e 's/27_paramLog.txt/ROMOND/g' -e 's/28_paramLog.txt/PACY/g'
}

get()
{
  grep $1 *appliLog.txt
}

get2()
{
  grep $1 *appliLog2.txt
}

get3()
{
  grep $1 *appliLog3.txt
}

getalllog()
{
	scp root@web-coonguu1.tst.fra.optiweb-access.com:/home/log/*.txt ~/testlog
}

