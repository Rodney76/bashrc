# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

alias rbrc='source ~/.bashrc'
alias brc='vi ~/.bashrc'

#alias alive='ls -al *paramLog.*'


export GREP_COLOR='2;17;1'
export GREP_COLORS='fn=1;32'


DISTRIBSALON=22099

VERNEUIL1=22031
VERNEUIL2=22032
VERNEUIL3=22034

BERGON1=22020
BERGON2=22021
BERGON3=22023

BONNARD1=22041
BONNARD2=22042
BONNARD3=22044

MARINES1=22036
MARINES2=22037
MARINES3=22038

PACY1=22051
PACY2=22052
PACY3=22054

BOUGUIN1=22061
BOUGUIN2=22062
BOUGUIN3=22064

ROMOND1=22071
ROMOND2=22072

ODINCOURT1=22081
ODINCOURT2=22082

DUNOY1=22091
DUNOY2=22092

CHAMPAGNOL1=22093
CHAMPAGNOL2=22094

SAINTWITZ1=22095

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

cd /home
source /home/down.sh

gethelp()
{
echo ""
echo "---- Commandes BASH pour environnement SERVEUR COONGUU OVH --------"
echo ""
echo "getall        : Liste de tous les ports reverse ssh"
echo "sendistrib $1 : Envois de NEW_DISTRIBUTEUR et READY vers le port ssh $1"
echo "lock $1       : ssh vers $1"
echo ""
echo "getversion    : Recuperation des versions logiciel des distributeurs installés"
echo "alive         : Liste des fichiers paramLog.txt (toutes les 30 minutes)"
echo "transac       : Liste des derniers appliLog.txt (transactions ou tentatives)"
echo "getname       : Conversion des noms distributeur vis a vis de leurs station_id"
echo "get  $1       : search for $1 dans *appliLog.txt"
echo "get2 $1       : idem dans *appliLog2.txt"
echo "get3 $1       : idem dans *appliLog3.txt"
echo ""
echo "-------------------------------------------------------------------"
echo ""
}

getall()
{
echo ""
echo "---- Liste des liaisons REVERSE SSH des distributeur installes --------"
echo ""
echo "DISTRIBSALON=22099"
echo ""
echo "VERNEUIL1=22031"
echo "VERNEUIL2=22032"
echo "VERNEUIL3=22034"
echo ""
echo "BERGON1=22020"
echo "BERGON2=22021"
echo "BERGON3=22023"
echo ""
echo "BONNARD1=22041"
echo "BONNARD2=22042"
echo "BONNARD3=22044"
echo ""
echo "MARINES1=22036"
echo "MARINES2=22037"
echo "MARINES3=22038"
echo ""
echo "PACY1=22051"
echo "PACY2=22052"
echo "PACY3=22054"
echo ""
echo "ROMOND1=22071"
echo "ROMOND2=22072"
echo ""
echo "BOUGUIN1=22061"
echo "BOUGUIN2=22062"
echo "BOUGUIN3=22064"
echo ""
echo "ODINCOURT1=22081"
echo "ODINCOURT2=22082"
echo ""
echo "DUNOY1=22091"
echo "DUNOY2=22092"
echo ""
echo "CHAMPAGNOL1=22093"
echo "CHAMPAGNOL2=22094"
echo ""
echo "SAINTWITZ1=22095"
echo "SAINTWITZ2=22096"
echo ""
echo "---------------------------------------------------------------------"
}


ttransac()
{
echo ""
ls -al *appliLog.* | sed -e 's/13_appliLog.txt/MARINES             /g' -e 's/22_appliLog.txt/LES ECUELLES        /g' -e 's/24_appliLog.txt/BERGON              /g' -e 's/27_appliLog.txt/ROMOND              /g' -e 's/28_appliLog.txt/PACY                /g' -e 's/18_appliLog.txt/ST-WITZ             /g' -e 's/30_appliLog.txt/BOUGUIN (IDM LAVAGE)/g'
echo ""
}


getversion()
{
echo ""
   grep --color=never g_version *paramLog.*  | sed -e 's/13_paramLog.txt:g_version_logiciel/MARINES             /g' -e 's/22_paramLog.txt:g_version_logiciel/LES ECUELLES        /g' -e 's/24_paramLog.txt:g_version_logiciel/BERGON              /g' -e 's/27_paramLog.txt:g_version_logiciel/ROMOND              /g' -e 's/28_paramLog.txt:g_version_logiciel/PACY                /g' -e 's/18_paramLog.txt:g_version_logiciel/ST-WITZ             /g' -e 's/30_paramLog.txt:g_version_logiciel/BOUGUIN (IDM LAVAGE)/g'
echo ""
}

getname()
{
  echo ""
  echo "13 : MARINES"
  echo "18 : ST-WITZ"
  echo "22 : LES ECUELLES (VERNEUIL)"
  echo "24 : BERGON (BOURMES-LES-MIMOSAS"
  echo "27 : ROMOND"
  echo "28 : JEDAV (PACY SUR EURE)"
  echo "30 : BOUGUIN (IDM LAVAGE)"
  echo ""
  echo " 6 : DEV"
  echo ""
}


alive()
{
        ls -al *paramLog.* | sed -e 's/13_paramLog.txt/MARINES/g' -e 's/22_paramLog.txt/LES ECUELLES/g' -e 's/24_paramLog.txt/BERGON/g' -e 's/27_paramLog.txt/ROMOND/g' -e 's/28_paramLog.txt/PACY/g' -e 's/18_paramLog.txt/ST-WITZ/g' -e 's/30_paramLog.txt/BOUGUIN (IDM LAVAGE)/g'
}

get()
{
  grep -n $1 *appliLog.txt
}

get2()
{
  grep -n $1 *appliLog2.txt
}

get3()
{
  grep -n $1 *appliLog3.txt
